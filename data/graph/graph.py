import pandas as pd
import matplotlib.pyplot as plt
import sys

figure = plt.gcf() # get current figure
figure.set_size_inches(20, 9)

d1 = pd.read_csv('http.1.0.csv')
d2 = pd.read_csv('http.1.1.csv')
d3 = pd.read_csv('http.2.0.csv')
#d4 = pd.read_csv('http.1.0_15.csv')
#d5 = pd.read_csv('http.1.1_15.csv')
#d6 = pd.read_csv('http.2.0_15.csv')

l1, = plt.plot(d1["timestamp"],d1["PackageEnergy"],'m')
l2, = plt.plot(d2["timestamp"],d2["PackageEnergy"],'y')
l3, = plt.plot(d3["timestamp"],d3["PackageEnergy"],'c')
#l4, = plt.plot(d4["timestamp"],d4["PackageEnergy"],'m--')
#l5, = plt.plot(d5["timestamp"],d5["PackageEnergy"],'y--')
#l6, = plt.plot(d6["timestamp"],d6["PackageEnergy"],'c--')
#plt.legend((l1,l4,l2,l5,l3,l6), 
#           ('http/1.0', 'http/1.0 1.5GHz', 'http/1.1', 'http/1.1 1.5GHz', 'http/2.0', 'http/2.0 1.5GHz',),
#           numpoints=1, loc='upper right', ncol=1, fontsize=12)
plt.legend((l1,l2,l3), 
           ('http/1.0', 'http/1.1', 'http/2.0'),
           numpoints=1, loc='upper right', ncol=1, fontsize=12)
plt.title(sys.argv[1])
plt.xlabel("Timestamp")
plt.ylabel("PackageEnergy")
plt.savefig(sys.argv[1]+ '.png', bbox_inches='tight')

#plt.show()

