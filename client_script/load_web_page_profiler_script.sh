#!/bin/bash

HWP_REQUEST_REG="0x$(sudo rdmsr 0x774 -p0)";
#Functions to set the IA32_HWP_REQUEST register values
#Takes one argument value
function setMinPerf
{
	#Set these two values based on the bitfield explanation
	local startPos=0
	local noOfBits=8
		
	local tmpMax=$1
	local MASK=0xFF
	local shiftBit=$(($noOfBits+$startPos-8))
	
	MASK=$(($MASK << $shiftBit))
	tmpMax=$(($tmpMax << $shiftBit))
	HWP_REQUEST_REG=$(($HWP_REQUEST_REG & ~$MASK))
	HWP_REQUEST_REG=$(($HWP_REQUEST_REG | $tmpMax))

}

#Takes one argument value
function setMaxPerf
{
	#Set these two values based on the bitfield explanation
	local startPos=8
	local noOfBits=8
		
	local tmpMax=$1
	local MASK=0xFF
	local shiftBit=$(($noOfBits+$startPos-8))
	#HWP_REQUEST_REG="0x$HWP_REQUEST_REG"
	
	MASK=$(($MASK << $shiftBit))
	tmpMax=$(($tmpMax << $shiftBit))
	HWP_REQUEST_REG=$(($HWP_REQUEST_REG & ~$MASK))
	HWP_REQUEST_REG=$(($HWP_REQUEST_REG | $tmpMax))
}

#Takes one argument value
function setDesiredPerf
{
	#Set these two values based on the bitfield explanation
	local startPos=16
	local noOfBits=8
		
	local tmpMax=$1
	local MASK=0xFF
	local shiftBit=$(($noOfBits+$startPos-8))
	
	MASK=$(($MASK << $shiftBit))
	tmpMax=$(($tmpMax << $shiftBit))
	HWP_REQUEST_REG=$(($HWP_REQUEST_REG & ~$MASK))
	HWP_REQUEST_REG=$(($HWP_REQUEST_REG | $tmpMax))
}

#Takes one argument value
function setEnergyPerf
{
	#Set these two values based on the bitfield explanation
	local startPos=24
	local noOfBits=8
		
	local tmpMax=$1
	local MASK=0xFF
	local shiftBit=$(($noOfBits+$startPos-8))
	#HWP_REQUEST_REG="0x$HWP_REQUEST_REG"
	
	MASK=$(($MASK << $shiftBit))
	tmpMax=$(($tmpMax << $shiftBit))
	HWP_REQUEST_REG=$(($HWP_REQUEST_REG & ~$MASK))
	HWP_REQUEST_REG=$(($HWP_REQUEST_REG | $tmpMax))
}

#Takes one argument value
function setActivityWindow
{
	#Set these two values based on the bitfield explanation
	local startPos=32
	local noOfBits=9
		
	local tmpMax=$1
	local MASK=0x01FF
	local shiftBit=$(($noOfBits+$startPos-16))
	
	MASK=$(($MASK << $shiftBit))
	tmpMax=$(($tmpMax << $shiftBit))
	HWP_REQUEST_REG=$(($HWP_REQUEST_REG & ~$MASK))
	HWP_REQUEST_REG=$(($HWP_REQUEST_REG | $tmpMax))
}

#Takes one argument value
function setPackageControl
{
	#Set these two values based on the bitfield explanation
	local startPos=42
	local noOfBits=8
		
	local tmpMax=$1
	local MASK=0x01
	local shiftBit=$(($noOfBits+$startPos - 8))
	
	MASK=$(($MASK << $shiftBit))
	tmpMax=$(($tmpMax << $shiftBit))
	HWP_REQUEST_REG=$(($HWP_REQUEST_REG & ~$MASK))
	HWP_REQUEST_REG=$(($HWP_REQUEST_REG | $tmpMax))
}


echo "Disabling NMI watchdog"
sudo sysctl kernel.nmi_watchdog=0


APPS[0]=sitea.com
APPS[1]=site-9GAG.com
APPS[2]=site-eBay.com
APPS[3]=site-Netflix.com
APPS[4]=site-StackOverflow.com
APPS[5]=site-WIRED.com
APPS[6]=site-Airbnb.com
APPS[7]=site-ESPN.com
APPS[8]=site-REDDIT.com
APPS[9]=site-TOI.com
APPS[10]=site-YouTube.com
APPS[11]=site-BBC.com
APPS[12]=site-NDTV.com
APPS[13]=site-Snapdeal.com
APPS[14]=site-Upworthy.com

apps_len=${#APPS[@]}


EVENTS[0]="0x004301D1,0x004308D1"; #L1_HIT,L1_MISS
EVENTS[1]="0x004302D1,0x004310D1"; #L2_HIT,L2_MISS
EVENTS[2]="0x004304D1,0x004320D1"; #L3_HIT,L3_MISS,
EVENTS[3]="0x004381D0,0x004382D0"; #ALL_LOADS,All_STORES
EVENTS[4]="0x0C430CA3,0x054305A3"; #CYCLES STALLS due to, "STALLS_L1D_MISS, STALLS_L2_MISS,
EVENTS[5]="0x064306A3,0x144314A3"; #CYCLES STALLS due to, "STALLS_L3_MISS,STALLS_MEM_ANY"

events_len=${#EVENTS[@]}

POWER_PROF_HOME=/home/jacksparrow/opt/src/PowerProfiler/bin
SAMPLING_RATE=10 #10ms sampling rate

MIN_PERF_VAL=("4")
#("9" "15" "20" "25" "30" "35" "40");
MAX_PERF_VAL=("31")
#("9" "15" "20" "25" "30" "35" "40");
DESIRED_PERF_VAL=("0" "15");
#("9" "15" "20" "25" "30" "35" "40");
ENERGY_PERF_PREFERENCE_VAL=("0")
#("0" "64" "128" "192" "255");
ACTIVITY_WINDOW_VAL=("0");

NUM_CORES=4

min_perf_len=${#MIN_PERF_VAL[@]}
max_perf_len=${#MAX_PERF_VAL[@]}
desired_perf_len=${#DESIRED_PERF_VAL[@]}
energy_perf_len=${#ENERGY_PERF_PREFERENCE_VAL[@]}
activity_window_len=${#ACTIVITY_WINDOW_VAL[@]}



EXP_HOME_DIR="/home/jacksparrow/Network_Project/"

RESULT_DIR="/home/jacksparrow/Network_Project/exp_raw_result/"

CHROME_SCRIPT_DIR="/home/jacksparrow/Network_Project/Script_from_Cononr/"


HTTP_VERSION=("http.1.0" "http.1.1" "http.2.0")

HTTP_PORT=("7000" "7100" "7200")

http_version_len=${#HTTP_VERSION[@]}


#Clear chrome cache before starting the experiment
# rm -rf ~/.config/google-chrome/Default/*
# rm -rf ~/.cache/google-chrome/*

#Run chrome
# google-chrome --user-data-dir --incognito --remote-debugging-port=9222 &

for o in `seq 0 $((${events_len}-1))`; do
	for hv in `seq 0 $((${http_version_len}-1))`; do
		for i in `seq 0 $((${min_perf_len}-1))`; do
			for j in `seq 0 $((${max_perf_len}-1))`; do
				
				# if [ "$i" != "$j" ]
				# then
					# continue;
				# fi
					

				for k in `seq 0 $((${desired_perf_len}-1))`; do
					#Run chrome
					google-chrome --incognito --remote-debugging-port=9222 &
					sleep 10

					for l in `seq 0 $((${energy_perf_len}-1))`; do
						for m in `seq 0 $((${activity_window_len}-1))`; do
							for n in `seq 0 $((${apps_len}-1))`; do
								#Write the Website name in the sites

								if [ $hv -eq 2 ]
								then
									echo -n ${APPS[$n]} > $CHROME_SCRIPT_DIR/sites.txt
	    
								else
									echo -n ${APPS[$n]}":"${HTTP_PORT[$hv]} > $CHROME_SCRIPT_DIR/sites.txt
								fi

								mkdir -p $RESULT_DIR/${HTTP_VERSION[$hv]}/${APPS[$n]}/
								echo "Running ${APPS[$n]}"

					
								echo "Min=" ${MIN_PERF_VAL[$i]} "Max=" ${MAX_PERF_VAL[$j]} "DesiredPerf=" ${DESIRED_PERF_VAL[$k]}  " EnergyPref=" ${ENERGY_PERF_PREFERENCE_VAL[$l]}
								#Set the value for all cores
								for p in `seq 0 $((${NUM_CORES}-1))`; do
									HWP_REQUEST_REG="0x$(sudo  rdmsr 0x774 -p$p)";
									
									echo "Before: Core-" $p ":" $HWP_REQUEST_REG
									# setMinPerf ${MIN_PERF_VAL[$i]};
									# setMaxPerf ${MAX_PERF_VAL[$j]};
									setDesiredPerf ${DESIRED_PERF_VAL[$k]};
									# setEnergyPerf ${ENERGY_PERF_PREFERENCE_VAL[$l]};
									#setActivityWindow ${ACTIVITY_WINDOW_VAL[$m]};
									
									sudo wrmsr 0x774 $HWP_REQUEST_REG -p$p;
									HWP_REQUEST_REG="0x$(sudo  rdmsr 0x774 -p$p)";
									
									echo "After: Core-" $p ":" $HWP_REQUEST_REG

								done

								sudo $POWER_PROF_HOME/PowerProfiler -o $RESULT_DIR/${HTTP_VERSION[$hv]}/${APPS[$n]}/${APPS[$n]}_PowerProfOutput_min_${MIN_PERF_VAL[$i]}_max_${MAX_PERF_VAL[$j]}_desired_${DESIRED_PERF_VAL[$k]}_energyPrefer_${ENERGY_PERF_PREFERENCE_VAL[$l]}_activityWindow_${ACTIVITY_WINDOW_VAL[$m]}_EventList_$o.csv -e ${EVENTS[$o]} -t $SAMPLING_RATE &
								
								
								#Commad to load the webpage
								cd $CHROME_SCRIPT_DIR

								if [ $hv -eq 2 ]
								then
									node chrome_metrics_https.js false 0 97500 41250 0    
								else
									node chrome_metrics.js false 0 97500 41250 0
								fi

								

								sudo killall PowerProfiler
								cd $EXP_HOME_DIR

								sleep 10
								
							done
						done
					done
					# sudo wrmsr 0x774 0x80001f04 -p0;
					#Close chrome
					# killall chrome
					# pkill -9 chrome 
					kill -9 `ps -ef |grep chrome| grep -v grep | awk -F" " '{print $2}'`
					sleep 60
				done
				
			done
		done
	done
done


for p in `seq 0 $((${NUM_CORES}-1))`; do
	
	sudo wrmsr 0x774 0x80001f04 -p$p;

done
