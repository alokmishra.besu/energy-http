#!/bin/bash

INPUT_PATH=/home/jacksparrow/Network_Project/exp_raw_result/http.2.0/*
#

EXP_PATH=/home/jacksparrow/Network_Project/exp_sum_result/http.2.0/
#
#ARRAY=(`find ${EXP_PATH}/4* -name profile_proc_full.csv | sort`)

for dir in $INPUT_PATH
do
	if [ -d "${dir}" ]
	then
	  echo "*****************Directory= "$dir" ***************"
	  for file in $dir/*
	  do
		echo $file
		declare $(echo $file | awk '{n = split($0, WORDS, "/");print "APP_NAME="WORDS[n]}')
		FINAL_TIME_STAMP=$(awk -F',' 'END {print FNR}' $file)
		echo $FINAL_TIME_STAMP
		cat $file | ./calculate_averages.awk -F',' $APP_NAME $FINAL_TIME_STAMP > $EXP_PATH/$APP_NAME
	  done
	fi
	#echo $dir
	
	
	#cat $f | ./calculate_averages.awk -F',' $f >tmp.csv &
	#cat tmp.csv | awk 'FNR > 1' tmp.csv > bigfile.csv &
	#rm tmp.csv;
done

cd $EXP_PATH
awk 'FNR==1 && NR!=1{next;}{print}' *.csv* >Final.avg.http.2.0


# function selRange {
    # node=$(hostname)
    # case $node in
	# node01)
	    # start=$(echo $((0*28)))
	    # end=$(echo $((5*28-1)))
	    # ;;
	# node02)
	    # start=$(echo $((5*28)))
	    # end=$(echo $((10*28-1)))
	    # ;;
	# node04)
	    # start=$(echo $((10*28)))
	    # end=$(echo $((17*28-1)))
	    # ;;
	# node05)
	    # start=$(echo $((17*28)))
	    # end=$(echo $((23*28-1)))
	    # ;;
	# node06)
	    # start=$(echo $((23*28)))
	    # end=$(echo $((28*28-1)))
	    # ;;
    # esac
# }

# selRange
# for app in `seq ${start} 4 ${end}`; do
    # for i in `seq 0 3`; do
	# curr_app=$(echo $(($app+$i)))
	# if [ ${curr_app} -gt ${end} ] ; then
	    # continue
	# fi
	# bmk=$(echo ${ARRAY[$curr_app]} | awk -F"/" '{print $6}')
	# CPU_PSTATE=$(echo ${ARRAY[$curr_app]} | awk -F"/" '{print $7}')
	# NB_PSTATE=$(echo ${ARRAY[$curr_app]} | awk -F"/" '{print $8}')
	# echo "cat ${ARRAY[$curr_app]} | ./calculate_averages.awk -F',' -M ${bmk} ${CPU_PSTATE} ${NB_PSTATE} > averages/${bmk}_${CPU_PSTATE}_${NB_PSTATE}.avg"
	# cat ${ARRAY[$curr_app]} | ./calculate_averages.awk -F',' -M ${bmk} ${CPU_PSTATE} ${NB_PSTATE} > averages/${bmk}_${CPU_PSTATE}_${NB_PSTATE}.avg &
    # done
    # wait
# done
